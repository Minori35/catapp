import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatsRoutingModule } from './cats-routing.module';
import { ListadoComponent } from './listado/listado.component';
import { HomeComponent } from './home/home.component';
import { CatComponent } from './cat/cat.component';
import { MaterialModule } from '../material/material.module';


import { MatSidenavModule } from '@angular/material/sidenav';
import { CatTarjetaComponent } from './components/cat-tarjeta/cat-tarjeta.component';
import { FilterPipe } from './pipe/filter.pipe';
import { FilterOriginPipe } from './pipe/filter-origin.pipe';


@NgModule({
  declarations: [ListadoComponent,  HomeComponent, CatComponent, CatTarjetaComponent],
  imports: [
    CommonModule,
    CatsRoutingModule,
    MaterialModule
  ],
  exports:[
    CatTarjetaComponent
  ]

})
export class CatsModule { }
