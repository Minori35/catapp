import { Pipe, PipeTransform } from '@angular/core';
import { Cat } from '../interfaces/cats.interface';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(cat: Cat[], page: number = 0, search :string =''): Cat[] {
    if(search.length === 0){

      console.log(search);
      return cat.slice(page,20)
    }
    console.log(search);
      const filterCat = cat.filter( cat => {
        console.log("🚀 ~ file: filter.pipe.ts:17 ~ FilterPipe ~ transform ~ cat", cat)
        return cat.name.includes(search)
      })
      return filterCat
  }

}
