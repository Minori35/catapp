import { Component, OnInit } from '@angular/core';
import { Cat } from '../../interfaces/cats.interface';
import { CatsService } from '../../services/cats.service';

@Component({
  selector: 'app-adapta',
  templateUrl: './adapta.component.html',
  styleUrls: ['./adapta.component.css']
})
export class AdaptaComponent implements OnInit {
  cat: Cat[] = [];
  cat1: any = {}
  cat2: any = {}
  cat3: any = {}
  cat4: any = {}
  cat5: any = {}
  cat6: any = {}
  cat7: any = {}
  cat8: any = {}
  cat9: any = {}
  cat10: any = {}
  cat11: any = {}
  cat12: any = {}
  constructor(private catsService: CatsService) { }

  ngOnInit(): void {
    this.cat= [];
    this.catsService.getCats(12).subscribe((data) => {
      // for (let index = 0; index < data.length; index++) {
      //   this.originCat.push(data[index].origin);
      //   this.nameCat.push(data[index].name);
      //   this.imgCat.push(data[index].image?.id);

      // }
      // for (let index = 0; index < this.imgCat.length; index++) {
      //   this.catsService.getImagen(data[index].image?.id).
      //     subscribe(data=> {
      //       this.urlImg.push(data.url)
      //     }
      //     )

      //   }
      this.cat=data;
      this.cat1=this.cat[0] ;
      this.cat2=this.cat[1] ;
      this.cat3=this.cat[2] ;
      this.cat4=this.cat[3] ;
      this.cat5=this.cat[4] ;
      this.cat6=this.cat[5] ;
      this.cat7=this.cat[6] ;
      this.cat8=this.cat[7] ;
      this.cat9=this.cat[8] ;
      this.cat10=this.cat[9] ;
      this.cat11=this.cat[10] ;
      this.cat12=this.cat[11] ;
      }
      );
  }

}
