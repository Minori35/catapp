import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriasRoutingModule } from './categorias-routing.module';
import { HomeComponent } from './home/home.component';
import { OrigenComponent } from './origen/origen.component';
import { CarinooComponent } from './carinoo/carinoo.component';
import { AdaptaComponent } from './adapta/adapta.component';
import { MaterialModule } from '../../material/material.module';


@NgModule({
  declarations: [HomeComponent, OrigenComponent, CarinooComponent, AdaptaComponent],
  imports: [
    CommonModule,
    CategoriasRoutingModule,
    MaterialModule
  ]
})
export class CategoriasModule { }
