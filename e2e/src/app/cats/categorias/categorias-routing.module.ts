import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { OrigenComponent } from './origen/origen.component';
import { CarinooComponent } from './carinoo/carinoo.component';
import { AdaptaComponent } from './adapta/adapta.component';

const routes: Routes = [
  {
    path:'',
    component:HomeComponent,
    children:[
      {path:'origen', component:OrigenComponent},
      {path:'carino', component:CarinooComponent},
      {path:'adaptabilidad', component:AdaptaComponent},
      {path:'**', redirectTo:'origen'}

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriasRoutingModule { }
