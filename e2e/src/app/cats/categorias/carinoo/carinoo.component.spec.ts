import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarinooComponent } from './carinoo.component';

describe('CarinooComponent', () => {
  let component: CarinooComponent;
  let fixture: ComponentFixture<CarinooComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarinooComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarinooComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
