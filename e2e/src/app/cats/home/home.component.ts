import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  styles: [`
  .container {
    margin: 10px;
  }
`]
})
export class HomeComponent implements OnInit {
  @ViewChild(MatMenuTrigger) trigger!: MatMenuTrigger;
  constructor() { }

  ngOnInit(): void {
  }

  someMethod() {
    this.trigger.openMenu();
  }

}
