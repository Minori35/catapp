import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cat } from '../interfaces/cats.interface';
import { CatsService } from '../services/cats.service';

import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-cat',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.css']
})
export class CatComponent implements OnInit {


  cat! : Cat;
  img:any;
  imgId: any;

  constructor(private router: Router,
              private catService: CatsService,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {

    this.getCat();

    // this.catService.getImagen(this.imgId)
    // .subscribe(url => this.img=url)
    // console.log("🚀 ~ file: cat.component.ts:37 ~ CatComponent ~ ngOnInit ~ this.img", this.img)


  }

  getCat(){
    this.activatedRouter.params
    .pipe(
      switchMap( ({ id }) => this.catService.getCatsPorId(id) )
      )
      .subscribe( cat => {
        this.cat = cat
        this.imgId= this.cat.reference_image_id
        this.catService.getImagen(this.imgId)
          .subscribe(data=> {
            this.img=data
            console.log("🚀 ~ file: cat.component.ts:48 ~ CatComponent ~ getCat ~ this.img", this.img.url)
          }
            )
    }
    );
  }

  regresar(){
    this.router.navigate([['/cats/listado']])
  }










}
