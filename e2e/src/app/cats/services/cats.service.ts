import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { Cat } from '../interfaces/cats.interface';
@Injectable({
  providedIn: 'root'
})
export class CatsService {
  private baseUrl : string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getCats(large : number): Observable<Cat[]>{
    return this.http.get<Cat[]>(`${ this.baseUrl }/v1/breeds?limit=${ large }`);
  }

  getCatsAll(): Observable<Cat[]>{
    return this.http.get<Cat[]>(`${ this.baseUrl }/v1/breeds`);
  }

  getCatsPorId(id :string): Observable<Cat>{
    console.log('ID',id);
    return this.http.get<Cat>(`${ this.baseUrl }/v1/breeds/${ id }`);

  }

  getImagen(id:any):Observable<any>{
    return this.http.get<any>(`${ this.baseUrl }/v1/images/${ id }`);
  }






}
