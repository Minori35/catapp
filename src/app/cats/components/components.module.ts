import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatTarjetaComponent } from './cat-tarjeta/cat-tarjeta.component';
import { CatsModule } from '../cats.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CatsModule
  ]
})
export class ComponentsModule { }
