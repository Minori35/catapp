import { Component, OnInit, Input} from '@angular/core';
import { Cat } from '../../interfaces/cats.interface';


@Component({
  selector: 'app-cat-tarjeta',
  templateUrl: './cat-tarjeta.component.html',
  styleUrls: ['./cat-tarjeta.component.css']
})
export class CatTarjetaComponent implements OnInit {

  @Input() cat! :Cat;
  constructor() { }

  ngOnInit(): void {
  }

}
