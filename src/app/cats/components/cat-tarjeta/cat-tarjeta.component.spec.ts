import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatTarjetaComponent } from './cat-tarjeta.component';

describe('CatTarjetaComponent', () => {
  let component: CatTarjetaComponent;
  let fixture: ComponentFixture<CatTarjetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatTarjetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
