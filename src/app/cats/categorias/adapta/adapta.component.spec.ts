import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdaptaComponent } from './adapta.component';

describe('AdaptaComponent', () => {
  let component: AdaptaComponent;
  let fixture: ComponentFixture<AdaptaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdaptaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdaptaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
