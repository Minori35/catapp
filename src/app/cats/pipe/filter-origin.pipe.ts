import { Pipe, PipeTransform } from '@angular/core';
import { Cat } from '../interfaces/cats.interface';

@Pipe({
  name: 'filterOrigin'
})
export class FilterOriginPipe implements PipeTransform {



  transform(cat: Cat[], page: number = 0, search :string =''): Cat[] {
    if(search.length === 0){

      console.log(search);
      return cat.slice(page,20)
    }
    console.log(search);
      const filterCat = cat.filter( cat => {
        return cat.origin.includes(search)
      })
      console.log("🚀 ~ file: filter-origin.pipe.ts:21 ~ FilterOriginPipe ~ filterCat ~ cat", cat)
      return filterCat
  }

}
