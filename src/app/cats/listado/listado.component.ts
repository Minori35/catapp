import { Component, OnInit } from '@angular/core';
import { CatsService } from '../services/cats.service';
import { Cat } from '../interfaces/cats.interface';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  cats : Cat[] = [];
  large:number =15;

  constructor( private catsServices: CatsService) { }

  ngOnInit(): void {
    this.imagen(this.large)
  }

  imagen(item:any){

    this.large=item;
    this.catsServices.getCats(this.large)
    .subscribe( cats => this.cats = cats)
    console.log("🚀 ~ file: listado.component.ts:25 ~ ListadoComponent ~ imagen ~ this.cats", this.cats)
    console.log("🚀 ~ file: listado.component.ts:25 ~ ListadoComponent ~ imagen ~ this.large", this.large)
  }







}
