import { Component, OnInit } from '@angular/core';
import { Cat } from '../../interfaces/cats.interface';
import { CatsService } from '../../services/cats.service';

@Component({
  selector: 'app-nombre',
  templateUrl: './nombre.component.html',
  styleUrls: ['./nombre.component.css']
})
export class NombreComponent implements OnInit {
  large:number =15;
  cats : Cat[] = [];
  search : string =''
  page: number =0;
  constructor( private catsServices : CatsService) { }

  ngOnInit(): void {
    this.imagen(this.large)
  }

  onSeachCat(search : string){
    this.search=search
  }

  imagen(item:any){
    this.large=item;
    this.catsServices.getCats(this.large)
    .subscribe( cats => this.cats = cats)
    console.log("🚀 ~ file: nombre.component.ts:23 ~ NombreComponent ~ imagen ~ this.cats", this.cats)
    console.log("🚀 ~ file: listado.component.ts:25 ~ ListadoComponent ~ imagen ~ this.large", this.large)
  }

  nextPage(){
    this.page +=6;
    console.log("🚀 ~ file: nombre.component.ts:30 ~ NombreComponent ~ nextPage ~  this.page",  this.page)
  }

  prevPage(){
    if( this.page >0){

      this.page -=6;
      console.log("🚀 ~ file: nombre.component.ts:37 ~ NombreComponent ~ prevPage ~ this.page", this.page)
    }
  }
}
