import { Component, Input, OnInit } from '@angular/core';
import { CatsService } from '../../services/cats.service';
import { Cat } from '../../interfaces/cats.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  cats : Cat[] = [];
  constructor(private catServices : CatsService) {

  }

  ngOnInit(): void {
    this.catServices.getCatsAll()
      .subscribe(cats => this.cats = cats);

  }



}
