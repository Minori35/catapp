import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { OrigenComponent } from './origen/origen.component';
import { NombreComponent } from './nombre/nombre.component';

const routes: Routes = [
  {
    path:'',
    component:HomeComponent,
    children:[
      {path:'origen', component:OrigenComponent},
      {path:'nombre', component:NombreComponent},
      {path:'**', redirectTo:'origen'}

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuscadorRoutingModule { }
