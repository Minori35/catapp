import { CatsModule } from './../cats.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuscadorRoutingModule } from './buscador-routing.module';
import { HomeComponent } from './home/home.component';
import { OrigenComponent } from './origen/origen.component';
import { NombreComponent } from './nombre/nombre.component';
import { MaterialModule } from '../../material/material.module';
import { ComponentsModule } from '../components/components.module';
import { CatTarjetaComponent } from '../components/cat-tarjeta/cat-tarjeta.component';
import { FilterPipe } from '../pipe/filter.pipe';
import { FilterOriginPipe } from '../pipe/filter-origin.pipe';


@NgModule({
  declarations: [FilterPipe, HomeComponent,  OrigenComponent, NombreComponent, FilterOriginPipe],
  imports: [
    CommonModule,
    BuscadorRoutingModule,
    MaterialModule,
    CatsModule
  ]
})
export class BuscadorModule { }
