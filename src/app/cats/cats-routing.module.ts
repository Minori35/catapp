import { BuscadorModule } from './buscar/buscador.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ListadoComponent } from './listado/listado.component';
import { CatComponent } from './cat/cat.component';

const routes: Routes = [
  {
    path:'',
    component:HomeComponent,
    children:[
      {path:'listado', component:ListadoComponent},
      {path:'buscar',
      loadChildren: () => import('./buscar/buscador.module').then( m => m.BuscadorModule )},
      {path:'categorias',
      loadChildren: () => import('./categorias/categorias.module').then( m => m.CategoriasModule )},
      {path:':id', component:CatComponent},
      {path:'**', redirectTo:'listado'}


    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatsRoutingModule { }
